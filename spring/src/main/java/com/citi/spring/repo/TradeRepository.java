package com.citi.spring.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<ObjectId, Trade>{

}