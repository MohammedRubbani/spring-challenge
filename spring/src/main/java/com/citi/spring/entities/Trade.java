package com.citi.spring.entities;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    private Date dateCreated;
    private String stockTicker;
    private double quantity;
    private double requestedPrice;
    private String tradeStatus;

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public Trade(Date dateCreated, String stockTicker, double quantity, double requestedPrice, String tradeStatus) {
        this.dateCreated = dateCreated;
        this.stockTicker = stockTicker;
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
        this.tradeStatus = tradeStatus;
    }

    public Trade() {
    }

}